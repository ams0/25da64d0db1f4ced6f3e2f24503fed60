#!/bin/bash

#Usage: k8sbuild <ACR> <REPO> <IMAGE> <TAG> <SOURCE (path to Dockerfile, defaults to ./Dockerfile)>

#Example: ./k8sbuild.sh theregistry ubuntu test 0.6


#Before running, create a docker-registry secret.

#kubectl create secret docker-registry theregistry --docker-username=${ACR} --docker-server=${ACR}.azurecr.io --docker-password=""


ACR=$1
REPO=$2
IMAGE=$3
TAG=$4

if test -z "$5" 
then
      SOURCE="./Dockerfile"
else
      SOURCE=$5
fi


kubectl create cm dockerfile-${ACR}-${REPO}-${TAG} --from-file=Dockerfile=${SOURCE}

kubectl apply -f - <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: kaniko-${IMAGE}-${TAG}
spec:
  restartPolicy: Never
  containers:
    - name: kaniko
      image: gcr.io/kaniko-project/executor:latest
      args:
        [
          "--dockerfile=/Dockerfile",
          "--context=/",
          "--destination=${ACR}.azurecr.io/${REPO}/${IMAGE}:${TAG}",
        ]
      volumeMounts:
        - name: ${ACR}
          mountPath: /kaniko/.docker/
        - name: docker-context
          mountPath: /Dockerfile
          subPath: Dockerfile
      resources:
        requests:
          cpu: 50m
          memory: 50Mi
        limits:
          cpu: 100m
          memory: 100Mi
  volumes:
    - name: ${ACR}
      secret:
        secretName: ${ACR}
        items:
          - key: .dockerconfigjson
            path: config.json
    - name: docker-context
      configMap:
        name: dockerfile-${ACR}-${REPO}-${TAG}
EOF



while kubectl get po kaniko-${IMAGE}-${TAG} | grep Completed ; ((ret=$?)) ;do
    echo "waiting for build to complete"; sleep 2
done

kubectl delete cm dockerfile-${ACR}-${REPO}-${TAG}

kubectl delete po kaniko-${IMAGE}-${TAG}

echo "Image building done"
echo "Your image is now available at ${ACR}.azurecr.io/${REPO}/${IMAGE}:${TAG}"
echo "docker pull ${ACR}.azurecr.io/${REPO}/${IMAGE}:${TAG}"
